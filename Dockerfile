FROM node:alpine AS builder

COPY ./server /server

WORKDIR /server

RUN npm install \
  && npm run build \
  && node dist/tools/init-data.js \
  && rm -rf node_modules

COPY ./web /web

WORKDIR /web

RUN npm install \
  && npm run build \
  && rm -rf node_modules

FROM node:alpine

EXPOSE 3030

WORKDIR /app

COPY --from=builder /server/dist /app
COPY --from=builder /server/package.json /app
COPY --from=builder /server/package-lock.json /app
COPY --from=builder /server/categories.csv /app
COPY --from=builder /server/bill.csv /app
COPY --from=builder /web/dist /app/public

RUN npm instal --production

CMD node index.js

