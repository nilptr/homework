# homework

## 启动预览

### 使用已构建好的 docker 镜像

（镜像包含 2000.01~2020.11 的模拟数据）

```bash
docker pull nilptrjs/homework

docker run -p 3030:3030 -it --rm nilptrjs/homework
```

访问 <http://localhost:3030> 即可

### 本地启动

启动后端服务

```bash
cd server

npm install

npm run build

# 准备数据
node dist/tools/init-data.js

node dist/index.js
# server is running at port 3030
```

启动前端 dev server

```bash
cd web

npm install

npm run dev
# devserver 端口为 3000
```

访问 <http://localhost:3000> 即可

## 如何测试

只对 Server 端存储层的代码编写了测试用例

```
cd server

npm run test
```

报错有可能是准备操作 beforeAll 执行完毕前就开始进行测试了，github issue 中有同类的问题：<https://github.com/facebook/jest/issues/9527> 。

使用 `jest.setTimeout` 调整了超时时间减少错误的发生，但测试时间也变得较长。

## 设计思路

数据操作约定好接口：

```typescript
export enum BillType {
  Outgo = 0,
  Income = 1,
}

export interface BillData {
  time: number,
  type: BillType,
  category?: string | null,
  amount: number,
}

export interface Bill extends BillData {
  id: string,
}

export interface Category {
  id: string;
  type: BillType,
  name: string,
}

export interface QueryOptions {
  from: number;
  to: number;

  categories?: string[] | null;

  pageNo?: number;
  pageSize?: number;
}

export interface QueryResult {
  total: number;
  pageNo: number;
  pageSize: number;
  list: Bill[];
}

export interface TypeSummary {
  amount: number;
  categories: {
    id: string;
    name: string;
    amount: number;
  }[];
}

export interface Summary {
  income: TypeSummary;
  outgo: TypeSummary;
}

export interface BillStore {
  // 读取 categories
  // or 缓存 bill 数据
  init(): Promise<void>;

  getCategories(): Category[];

  getCategoryName(id: string): string;

  // 添加账单，返回 id
  addBill(data: BillData): Promise<string>;

  query(options: QueryOptions): Promise<QueryResult>;

  getSummary(from: number, to: number): Promise<Summary>;
}
```

存储层设计详见 [存储层分析](./doc/存储层分析.md)

表现层就前端页面没什么特别的，由于是小作业就尝试了下 Vue3 + vite ，所有组件纯手撸，除了 vue/axios/@popperjs/g2plot 没有别的依赖。

## 开发过程中的一些 issue

- <https://github.com/vuejs/docs-next/issues/694>
- <https://github.com/vitejs/vite/issues/1094>
- <https://github.com/antvis/g/issues/657>
- <https://github.com/antvis/g/pull/658#issuecomment-731552575>
