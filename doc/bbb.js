
// const { promises: fs } = require('fs');

// (async () => {
//   let data = null;

//   for (let i = 0; i < 10; ++i) {
//     console.time(`read ${i}`);
//     data = await fs.readFile('./bill-300.csv');
//     console.timeEnd(`read ${i}`);
//   }

//   for (let i = 0; i < 10; ++i) {
//     console.time(`write ${i}`);
//     await fs.writeFile('./bill-300.csv', data);
//     console.timeEnd(`write ${i}`);
//   }
// })().catch(console.error);

const arr = [];

const num = 3 * 1024 * 1024;

for (let i = 0; i < num; ++i) {
  arr.push(Math.floor(Math.random() * 10000));
}

for (let i = 0; i < 10; ++i) {
  console.time(`sort-${i}`);
  arr.sort();
  console.timeEnd(`sort-${i}`);
}

console.log('--------------------------------------');

function binarySearchInsertPosition(arr, k) {
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    const mid = low + Math.floor((high - low) / 2);

    if (arr[mid] === k) {
      let idx = mid;
      for (; arr[idx] === k; ++idx);
      return idx;
    }

    if (arr[mid] < k) {
      low = mid + 1;
    } else if (arr[mid] > k) {
      high = mid - 1;
    }
  }

  return high + 1; // = return low; ?
}

function insert(num) {
  console.time('search');
  const idx = binarySearchInsertPosition(arr, num);
  console.timeEnd('search');
  console.log(idx);
  console.time('insert');
  arr.splice(idx, 0, num);
  console.timeEnd('insert');
}

for (let i = 0; i < 30; ++i) {
  const ran = Math.floor(Math.random() * 10000);

  insert(ran);
}

console.log(process.memoryUsage());
