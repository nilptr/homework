module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint',
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.mjs', '.ts', '.d.ts'],
      },
    },
  },
  rules: {
    'no-plusplus': ['error', {
      allowForLoopAfterthoughts: true,
    }],

    'no-multiple-empty-lines': ['error', {
      max: 2,
      maxBOF: 2,
      maxEOF: 2,
    }],

    // https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-vars.md
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'error',

    // https://github.com/typescript-eslint/typescript-eslint/issues/2483
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': 'error',

    'import/extensions': ['error', 'always', {
      js: 'never',
      mjs: 'never',
      ts: 'never',
      'd.ts': 'never',
    }],
  },
};
