/* global describe, expect, test */

import { binarySearch, binarySearchInsertPosition } from '../libs/binary-search';

const cmp = (a: number, b: number): number => a - b;

describe('test binarySearch', () => {
  test('basic function', () => {
    const arr = [1, 3, 5, 7, 9];

    arr.forEach((target, result) => {
      expect(binarySearch(arr, target, cmp)).toBe(result);
    });
  });

  test('return -1 if no target value found', () => {
    const arr = [1, 3, 5, 7, 9];

    [2, 4, 6, 8, 10].forEach((target) => {
      expect(binarySearch(arr, target, cmp)).toBe(-1);
    });
  });

  test('test with array of duplicated values', () => {
    const arr = [1, 1, 1, 3, 3, 3, 3, 5, 5, 7, 7, 7, 7, 7, 9, 9, 9, 9];

    [1, 3, 5, 7, 9].forEach((target) => {
      expect(binarySearch(arr, target, cmp))
        .toBe(arr.findIndex((v) => v === target));
    });
  });

  test('test with array of duplicated values - trailing', () => {
    const arr = [1, 1, 1, 3, 3, 3, 3, 5, 5, 7, 7, 7, 7, 7, 9, 9, 9, 9];

    [
      [1, 2],
      [3, 6],
      [5, 8],
      [7, 13],
      [9, 17],
    ].forEach(([target, result]) => {
      expect(binarySearch(arr, target, cmp, true)).toBe(result);
    });
  });
});

describe('test binarySearchInsertPosition', () => {
  test('insert an existing value', () => {
    const arr = [1, 3, 5, 7, 9];

    arr.forEach((target, result) => {
      expect(binarySearchInsertPosition(arr, target, cmp)).toBe(result);
    });
  });

  test('insert a new value that does not exist in the array', () => {
    const arr = [1, 3, 5, 7, 9];

    expect(binarySearchInsertPosition(arr, 0, cmp)).toBe(0);

    [2, 4, 6, 8, 10].forEach((target, index) => {
      expect(binarySearchInsertPosition(arr, target, cmp)).toBe(index + 1);
    });
  });

  test('test with array of duplicated values', () => {
    const arr = [1, 1, 1, 3, 3, 3, 3, 5, 5, 7, 7, 7, 7, 7, 9, 9, 9, 9];

    [1, 3, 5, 7, 9].forEach((target) => {
      expect(binarySearch(arr, target, cmp))
        .toBe(arr.findIndex((v) => v === target));
    });

    expect(binarySearchInsertPosition(arr, 0, cmp)).toBe(0);

    [
      [2, 3],
      [4, 7],
      [6, 9],
      [8, 14],
      [10, 18],
    ].forEach(([target, result]) => {
      expect(binarySearchInsertPosition(arr, target, cmp)).toBe(result);
    });
  });

  test('test trailing option', () => {
    const arr = [1, 3, 5, 7, 9];

    expect(binarySearchInsertPosition(arr, 0, cmp, true)).toBe(0);

    [2, 4, 6, 8, 10].forEach((target, index) => {
      expect(binarySearchInsertPosition(arr, target, cmp, true)).toBe(index + 1);
    });

    arr.forEach((target, result) => {
      expect(binarySearchInsertPosition(arr, target, cmp, true)).toBe(result + 1);
    });

    const arr2 = [1, 1, 1, 3, 3, 5, 5, 5, 5, 7, 7, 7, 9, 9, 9, 9, 9];

    [
      [0, 0],
      [1, 3],
      [2, 3],
      [3, 5],
      [4, 5],
      [5, 9],
      [6, 9],
      [7, 12],
      [8, 12],
      [9, 17],
      [10, 17],
    ].forEach(([target, result]) => {
      expect(binarySearchInsertPosition(arr2, target, cmp, true)).toBe(result);
    });
  });
});
