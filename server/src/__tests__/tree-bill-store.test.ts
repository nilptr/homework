/* global jest, beforeAll, afterAll, describe, expect, test */
/* eslint-disable no-await-in-loop */

import * as os from 'os';
import * as fs from 'fs';
import * as path from 'path';

import { parse } from 'csv';

import { Bill, BillType, Category } from '../types';
import BillStore from '../libs/tree-server-bill-store/server-bill-store';
import flushBills from '../libs/flush-bills';
import { generateSimulatedData } from '../tools/bill-generator';

jest.setTimeout(20000);

const categoriesCSVContent = `id,type,name
1bcddudhmh,0,车贷
hc5g66kviq,0,车辆保养
8s0p77c323,0,房贷
0fnhbcle6hg,0,房屋租赁
odrjk823mj8,0,家庭用品
bsn20th0k2o,0,交通
j1h1nohhmmo,0,旅游
3tqndrjqgrg,0,日常饮食
s73ijpispio,1,工资
1vjj47vpd28,1,股票投资
5il79e11628,1,基金投资`;

const billCSVContent = `id,type,time,category,amount
w_iu4aOuGrjmpSrFiJ8c6,0,1561910400000,8s0p77c323,540000
MR-v3N6nlG84tuksn3nQM,0,1561910400000,0fnhbcle6hg,150000
02BQAg0Tq25VHKzm4yxCv,0,1563897600000,3tqndrjqgrg,390000
qmMCGfnRMS-6koZ3hk6wo,0,1564502400000,bsn20th0k2o,190000
njcFheyVtzk85ruho6wqa,0,1564588800000,8s0p77c323,540000
rh-5T4zOAhHjjfCsujTk7,0,1564588800000,0fnhbcle6hg,150000
2lrSLjjnPaTjU442ruOqY,0,1564588800000,3tqndrjqgrg,500000
1LIFtHvwhjUW_kSaaPZJk,0,1566316800000,bsn20th0k2o,200000
4k6ZCEe20hfLCWyZ9tQiC,0,1567267200000,8s0p77c323,540000
BFwXfD4DuwKWla8k35aER,0,1567267200000,0fnhbcle6hg,150000
fKMbIQ6KM9Hul4OSFSIDZ,0,1569772800000,1bcddudhmh,300000
uuKKTHAbnynxHzEun0j68,0,1569772800000,bsn20th0k2o,150000
XCmCw4ICker6mNBq7SZAq,0,1569772800000,3tqndrjqgrg,500000
bxXpy5subNG_k9fN8lLQe,0,1569859200000,0fnhbcle6hg,150000
O0M5CgEJs9o-D1sw_isvZ,0,1572364800000,odrjk823mj8,300000
mbRJz-3j2zQvSF5l0VKd0,0,1572451200000,3tqndrjqgrg,460000
vERf7F045qRLWbEDQ7-xQ,0,1572451200000,3tqndrjqgrg,380000
sBp5AM5PF0wS6L3ESTDFw,0,1572537600000,0fnhbcle6hg,150000
GIvEq0uvlIlupza45fCyu,0,1574179200000,odrjk823mj8,200000
sNpkS4gCedX5limQnAqdZ,0,1574870400000,1bcddudhmh,300000
IhtxKUvSUNNzIdNVFYy6c,0,1574956800000,8s0p77c323,540000
uu9sGxwvU8VJa5IzUIJyF,0,1575043200000,3tqndrjqgrg,500000
eNBa_Qs2I3HDvZJtGcZni,0,1575129600000,0fnhbcle6hg,150000
8gEdxjXbnKRDPXAKtSA9y,0,1577289600000,3tqndrjqgrg,400000
poT5eYTejOLGYUQUtFpVn,0,1577345333184,odrjk823mj8,200000
2yOx_LNZ50p60KdHKxJcu,0,1577345367638,1bcddudhmh,300000
_0bEKOmAvZdMbAgIi0N9t,0,1577345378418,j1h1nohhmmo,80000
_693DiidG87IOHmLEb2Tn,0,1577345504140,bsn20th0k2o,100000
9cgAS8fBd1nOs_tfr5WO0,0,1577345517217,hc5g66kviq,200000
YizvlazuYVa6uGlLRJa-q,0,1577345576917,8s0p77c323,540000
i4oFgYkHd7JR0ChN8_aG9,0,1577345590283,1bcddudhmh,300000
7NAq8a1hyC2H4RviYGLeE,0,1577345789527,3tqndrjqgrg,390000
zQ2E2QhVqTkaB5CsDwFW-,0,1577548800000,8s0p77c323,540000
zkrDvjs1b6zRw9ukjEAW3,1,1561910400000,s73ijpispio,3000000
rP53lproptWYjl4sixxrH,1,1564502400000,5il79e11628,100000
ZJVT_WGxzxVCK3-ZkueOp,1,1567094400000,1vjj47vpd28,300000
2m9rTAUnaXbqLapEUqSSE,1,1567180800000,s73ijpispio,2800000
GVvvo8QtwFMWbRWGyUTVY,1,1569772800000,s73ijpispio,2800000
UJGR3WAnqQwSEWh3P0OPI,1,1569772800000,1vjj47vpd28,200000
RD7eZVM4FxgqWaL6Jebyc,1,1572451200000,s73ijpispio,2000000
hG0NhRep8TKyYtUE2PzLT,1,1577345267529,s73ijpispio,3000000
7uIiGSOeYv4FWtZ4xQX_t,1,1577345303191,1vjj47vpd28,1000000
9yglbmYuLF_isVI-lbKPh,1,1577345317187,5il79e11628,100000
nZQL5KG10Kyk3cu65Lu35,1,1577345463930,s73ijpispio,300000
JpPuu-wzv7Gd364ZPHbsz,1,1577345477581,5il79e11628,200000
QM63Z6Ub1OqcbbHnLLcUG,1,1577345638784,1vjj47vpd28,200000
`;

let tmpdir: string;
let billCSVPath: string;
let categoriesCSVPath: string;
let dataSetPath: string;

let categories: Category[];

let start: number;
let end: number;
let dataset: Bill[];

let store: BillStore;

function randomNumber(min: number, max: number): number {
  return min + Math.floor(Math.random() * (max - min));
}

function randomPick<T>(arr: T[]): T {
  if (arr.length === 0) {
    throw new TypeError('arr is empty');
  }
  return arr[randomNumber(0, arr.length)];
}

beforeAll(async () => {
  tmpdir = await fs.promises.mkdtemp(path.join(os.tmpdir(), 'bill-store-test-'));

  billCSVPath = path.join(tmpdir, 'bill.csv');
  categoriesCSVPath = path.join(tmpdir, 'categories.csv');

  dataSetPath = path.join(tmpdir, 'dataset.csv');

  [categories] = await Promise.all([
    new Promise<Category[]>((resolve, reject) => {
      parse(categoriesCSVContent, {
        trim: true,
        columns: true,
        skipEmptyLines: true,
        cast(value, { column }) {
          if (column === 'type') {
            return parseInt(value, 10);
          }
          return value;
        },
      }, (err, result) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(result);
      });
    }),
    fs.promises.writeFile(billCSVPath, billCSVContent),
    fs.promises.writeFile(categoriesCSVPath, categoriesCSVContent),
  ]);

  const d1 = new Date(2000, 0, 1);
  const d2 = new Date(2040, 0, 1);

  start = d1.getTime();
  end = d2.getTime();

  dataset = generateSimulatedData(d1, d2).sort((a, b) => a.time - b.time);

  await flushBills(dataSetPath, dataset);

  store = new BillStore(dataSetPath, categoriesCSVPath);
  await store.init();
});

afterAll(async () => {
  // console.log(tmpdir);
  await fs.promises.rmdir(tmpdir, { recursive: true });
});

describe('test TreeBillStore', () => {
  test('test BillStore#getCategories()', () => {
    expect(store.getCategories()).toEqual(categories);
  });

  test('test BillStore#getCategoryName()', () => {
    categories.forEach(({ id, name }) => {
      expect(store.getCategoryName(id)).toBe(name);
    });

    expect(store.getCategoryName('!!!@#$#@!!!')).toBe('');
    expect(store.getCategoryName('(*****)())))))')).toBe('');
    expect(store.getCategoryName('~~~~~{{}{}{{{')).toBe('');
  });

  test('test BillStore#query()', async () => {
    // 10 个 随机 case
    for (let i = 0; i < 10; ++i) {
      const [from, to] = [randomNumber(start, end), randomNumber(start, end)]
        .sort((a: number, b: number) => a - b);

      // console.log(`i=${i}`);
      // console.log(`from = ${from}, to = ${to}`);

      const result = dataset.filter((bill) => bill.time >= from && bill.time < to);

      const pageSize = 10 + Math.floor(Math.random() * 10);

      for (let j = 1; (j - 1) * pageSize < result.length; ++j) {
        const ret = await store.query({
          from,
          to,
          pageNo: j,
          pageSize,
        });

        expect(ret.total).toBe(result.length);
        expect(ret.pageNo).toBe(j);
        expect(ret.pageSize).toBe(pageSize);
        expect(ret.list).toEqual(result.slice((j - 1) * pageSize, j * pageSize));
      }
    }
    // 10 个带 categories 随机 case
    for (let i = 0; i < 10; ++i) {
      const [from, to] = [randomNumber(start, end), randomNumber(start, end)]
        .sort((a: number, b: number) => a - b);

      const allCategories = [
        ...categories.map((c) => c.id),
        'uncategorized-income',
        'uncategorized-outgo',
      ];

      const cates = [];
      const count = randomNumber(0, 4);
      for (let j = 0; j < count; ++j) {
        cates.push(randomPick(allCategories));
      }

      const set = new Set(cates);
      const result = dataset.filter((bill) => (
        bill.time >= from
        && bill.time < to
        && set.has(bill.category || (bill.type ? 'uncategorized-income' : 'uncategorized-outgo'))
      ));

      const pageSize = 10 + Math.floor(Math.random() * 10);

      for (let j = 1; (j - 1) * pageSize < result.length; ++j) {
        const ret = await store.query({
          from,
          to,
          categories: cates,
          pageNo: j,
          pageSize,
        });

        expect(ret.total).toBe(result.length);
        expect(ret.pageNo).toBe(j);
        expect(ret.pageSize).toBe(pageSize);
        expect(ret.list).toEqual(result.slice((j - 1) * pageSize, j * pageSize));
      }
    }
  });

  test('test persistence', async () => {
    const tmpStore = new BillStore(billCSVPath, categoriesCSVPath);
    await tmpStore.init();

    const data1 = {
      time: Date.now(),
      type: BillType.Income,
      category: randomPick(categories).id,
      amount: 1000,
    };
    const data2 = {
      time: Date.now(),
      type: BillType.Outgo,
      category: randomPick(categories).id,
      amount: 2000,
    };

    const id1 = await tmpStore.addBill(data1);
    const id2 = await tmpStore.addBill(data2);

    const content = await fs.promises.readFile(billCSVPath, { encoding: 'utf-8' });

    const bills = await new Promise((resolve, reject) => {
      parse(content, {
        delimiter: ',',
        trim: true,
        columns: true,
        skipEmptyLines: true,
        cast(value, { column }) {
          if (column === 'type' || column === 'time' || column === 'amount') {
            return parseInt(value, 10);
          }
          if (column === 'category') {
            return value || null;
          }
          return value;
        },
      }, (err, result) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(result);
      });
    });

    expect(bills).toContainEqual({ id: id1, ...data1 });
    expect(bills).toContainEqual({ id: id2, ...data2 });
  });
});
