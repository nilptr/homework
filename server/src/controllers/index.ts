import { Context } from 'koa';

import { AppContext, BillData, BillType } from '../types';

export async function addBill(ctx: Context & AppContext) {
  const { body } = ctx.request;

  if (typeof body.time !== 'number') {
    ctx.body = {
      code: 400,
      msg: '非法参数 time',
    };
    return;
  }

  if (body.type !== BillType.Income && body.type !== BillType.Outgo) {
    ctx.body = {
      code: 400,
      msg: '非法参数 type',
    };
    return;
  }

  if (typeof body.amount !== 'number') {
    ctx.body = {
      code: 400,
      msg: '非法参数 amount',
    };
    return;
  }

  if (body.category) {
    const categories = ctx.store.getCategories();
    const cate = categories.find((c) => c.id === body.category);
    if (!cate) {
      ctx.body = {
        code: 400,
        msg: '非法参数 category',
      };
      return;
    }
    if (cate.type !== body.type) {
      ctx.body = {
        code: 400,
        msg: 'category 与 type 不匹配',
      };
      return;
    }
  }

  const id = await ctx.store.addBill(body as BillData);

  ctx.body = {
    code: 0,
    data: id,
  };
}

export async function getBill(ctx: Context & AppContext) {
  const { query } = ctx;

  if (!(query.from && query.to)) {
    ctx.body = {
      code: 400,
      msg: '缺少参数',
    };
    return;
  }

  const from = parseInt(query.from, 10);
  const to = parseInt(query.to, 10);

  if (Number.isNaN(from)) {
    ctx.body = {
      code: 400,
      msg: '非法参数 from',
    };
    return;
  }
  if (Number.isNaN(to)) {
    ctx.body = {
      code: 400,
      msg: '非法参数 to',
    };
    return;
  }

  if (from >= to) {
    ctx.body = {
      code: 400,
      msg: '非法参数 from >= to',
    };
    return;
  }

  const categories = query.categories ? query.categories.split(',') : null;

  const pageNo = parseInt(query.pageNo, 10) || 1;
  const pageSize = parseInt(query.pageSize, 10) || 10;

  console.time('query');
  const data = await ctx.store.query({
    from,
    to,
    categories,
    pageNo,
    pageSize,
  });
  console.timeEnd('query');

  ctx.body = {
    code: 0,
    data,
  };
}

export async function getSummary(ctx: Context & AppContext) {
  const { query } = ctx;

  if (!(query.from && query.to)) {
    ctx.body = {
      code: 400,
      msg: '缺少参数',
    };
    return;
  }

  const from = parseInt(query.from, 10);
  const to = parseInt(query.to, 10);

  if (Number.isNaN(from)) {
    ctx.body = {
      code: 400,
      msg: '非法参数 from',
    };
    return;
  }
  if (Number.isNaN(to)) {
    ctx.body = {
      code: 400,
      msg: '非法参数 to',
    };
    return;
  }

  console.time('summary');
  const summary = await ctx.store.getSummary(from, to);
  console.timeEnd('summary');

  ctx.body = {
    code: 0,
    data: summary,
  };
}

export function getCategories(ctx: Context & AppContext) {
  ctx.body = {
    code: 0,
    data: ctx.store.getCategories(),
  };
}
