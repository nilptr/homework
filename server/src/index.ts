import * as path from 'path';

import * as Koa from 'koa';
import * as serve from 'koa-static';
import * as bodyParser from 'koa-bodyparser';

import { AppContext } from './types';

import ServerBillStore from './libs/tree-server-bill-store/server-bill-store';

import catchError from './middlewares/catch-error';

import router from './router';

const cwd = process.cwd();

(async () => {
  const billCSVPath = path.resolve(cwd, './bill.csv');
  const categoryCSVPath = path.resolve(cwd, './categories.csv');

  const store = new ServerBillStore(billCSVPath, categoryCSVPath);

  const t = Date.now();

  await store.init();

  console.log(`init finished in ${Date.now() - t}ms`);

  const app = new Koa<any, AppContext>();

  app.context.store = store;

  app.use(catchError);

  app.use(serve(path.resolve(cwd, './public')));

  app.use(bodyParser());

  app.use(router.routes());
  app.use(router.allowedMethods());

  app.listen(3030, () => {
    console.log('server is running at port 3030');
  });
})().catch(console.error);
