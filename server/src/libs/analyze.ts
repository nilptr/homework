import {
  Bill,
  BillType,
  Category,
  Summary,
} from '../types';

function analyze(bills: Bill[], categoriesMap: Map<string, Category>): Summary {
  const summaryMap = new Map<string, number>();

  let incomeAmount = 0;
  let outgoAmount = 0;

  let uncategorizedIncomeAmount = 0;
  let uncategorizedOutgoAmount = 0;

  for (let i = 0; i < bills.length; ++i) {
    const bill = bills[i];

    if (bill.category) {
      if (summaryMap.has(bill.category)) {
        const prevAmount = summaryMap.get(bill.category)!;
        summaryMap.set(bill.category, prevAmount + bill.amount);
      } else {
        summaryMap.set(bill.category, bill.amount);
      }
    }

    if (bill.type === BillType.Income) {
      incomeAmount += bill.amount;
      if (!bill.category) {
        uncategorizedIncomeAmount += bill.amount;
      }
    } else {
      outgoAmount += bill.amount;
      if (!bill.category) {
        uncategorizedOutgoAmount += bill.amount;
      }
    }
  }

  const income = [];
  const outgo = [];

  const entries = Array.from(summaryMap);
  for (let i = 0; i < entries.length; ++i) {
    const [id, amount] = entries[i];

    const category = categoriesMap.get(id)!;

    if (category.type === BillType.Income) {
      income.push({
        ...category,
        amount,
      });
    } else {
      outgo.push({
        ...category,
        amount,
      });
    }
  }

  income.push({
    id: 'uncategorized-income',
    name: '未分类',
    amount: uncategorizedIncomeAmount,
  });
  outgo.push({
    id: 'uncategorized-outgo',
    name: '未分类',
    amount: uncategorizedOutgoAmount,
  });

  return {
    income: {
      amount: incomeAmount,
      categories: income,
    },
    outgo: {
      amount: outgoAmount,
      categories: outgo,
    },
  };
}

export default analyze;
