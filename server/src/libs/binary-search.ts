
type Comparator<T> = (a: T, b: T) => number;

export function binarySearch<T>(
  arr: T[],
  target: T,
  cmp: Comparator<T>,
  trailing?: boolean,
): number {
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    const mid = low + Math.floor((high - low) / 2);

    const tmp = cmp(arr[mid], target);

    if (tmp === 0) {
      let idx = mid;

      if (trailing) {
        while (idx < arr.length - 1 && cmp(arr[idx + 1], target) === 0) {
          idx += 1;
        }
      } else {
        while (idx > 0 && cmp(arr[idx - 1], target) === 0) {
          idx -= 1;
        }
      }

      return idx;
    }

    if (tmp < 0) {
      low = mid + 1;
    } else if (tmp > 0) {
      high = mid - 1;
    }
  }

  return -1;
}

export function binarySearchInsertPosition<T>(
  arr: T[],
  target: T,
  cmp: Comparator<T>,
  trailing?: boolean,
): number {
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    const mid = low + Math.floor((high - low) / 2);

    const tmp = cmp(arr[mid], target);

    if (tmp === 0) {
      let idx = mid;

      if (trailing) {
        for (; idx < arr.length && cmp(arr[idx], target) === 0; idx++);
      } else {
        for (; idx > 0 && cmp(arr[idx - 1], target) === 0; idx--);
      }

      return idx;
    }

    if (tmp < 0) {
      low = mid + 1;
    } else if (tmp > 0) {
      high = mid - 1;
    }
  }

  return low;
}
