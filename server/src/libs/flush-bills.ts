
import * as fs from 'fs';

import { Bill } from '../types';

const columns = ['id', 'type', 'time', 'category', 'amount'] as (keyof Bill)[];

function isEmpty(value: any): boolean {
  return value === null || value === undefined;
}

function generateBillRow(bill: Bill): string {
  return `${columns.map((k) => (isEmpty(bill[k]) ? '' : bill[k]))}\n`;
}

export default function flush(path: string, bills: Bill[]): Promise<void> {
  return new Promise((resolve, reject) => {
    const dist = fs.createWriteStream(path);

    dist.on('error', reject);

    let i = 0;
    function write() {
      let ok = true;
      while (i < bills.length && ok) {
        const bill = bills[i];
        const data = generateBillRow(bill);

        if (i === bills.length - 1) { // 最后一行
          dist.end(data, resolve);
          return;
        }

        ok = dist.write(data);

        i += 1;
      }

      if (i < bills.length) {
        dist.once('drain', write);
      }
    }

    if (dist.write(`${columns.join(',')}\n`)) {
      write();
    } else {
      dist.once('drain', write);
    }
  });
}
