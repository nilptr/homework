import * as fs from 'fs';

import { parse } from 'csv';
import { Options as ParseOptions } from 'csv-parse';
import { nanoid } from 'nanoid';

import {
  BillStore,
  Category,
  BillData,
  Bill,
  QueryOptions,
  Summary,
} from '../types';
import { binarySearchInsertPosition } from './binary-search';
import analyze from './analyze';
import flushBills from './flush-bills';

function readCSV<T>(path: string, options?: ParseOptions): Promise<T[]> {
  return new Promise((resolve, reject) => {
    const stream = parse({
      ...options,
      delimiter: ',',
      trim: true,
      columns: true,
      skipEmptyLines: true,
    }, (err, records) => {
      // 传 onRecord 时 records 为 onRecord 返回值
      // onRecord 返回值为 falsy 时不会记录到 records 中
      if (err) {
        reject(err);
      } else {
        resolve(records);
      }
    });

    fs.createReadStream(path, {
      encoding: 'utf-8',
    }).pipe(stream);
  });
}

function compareBillTime(a: Bill, b: Bill): number {
  return a.time - b.time;
}

// function compareBillAmount(a: Bill, b: Bill): number {
//   return a.amount - b.amount;
// }

function getBillCategory(bill: Bill): string {
  return bill.category || (bill.type
    ? 'uncategorized-income'
    : 'uncategorized-outgo');
}

const defaultPageNo = 1;
const defaultPageSize = 10;

export default class ServerBillStore implements BillStore {
  private readonly billCSVPath: string;

  private readonly categoriesCSVPath: string;

  private bills: Bill[];

  private billsMap: Map<string, Bill>;

  private categories: Category[];

  private categoriesMap: Map<string, Category>;

  constructor(billCSVPath: string, categoriesCSVPath: string, idleTime: number = 30 * 1000) {
    this.billCSVPath = billCSVPath;
    this.categoriesCSVPath = categoriesCSVPath;

    this.categories = [];
    this.categoriesMap = new Map();

    this.bills = [];
    this.billsMap = new Map<string, Bill>();

    this.idleTime = idleTime;

    this.timeoutId = null;
    this.flushing = false;
    this.pending = false;
  }

  public async init(): Promise<void> {
    await Promise.all([
      this.loadBills(),
      this.loadCategories(),
    ]);
  }

  public getCategories(): Category[] {
    return this.categories;
  }

  public getCategoryName(id: string): string {
    return this.categoriesMap.get(id)?.name || '';
  }

  public async addBill(data: BillData): Promise<string> {
    const id = nanoid();

    const bill = {
      ...data,
      id,
    };

    const idx = binarySearchInsertPosition<Bill>(this.bills, bill, compareBillTime, true);

    // console.time('splice');
    this.bills.splice(idx, 0, bill);
    // console.timeEnd('splice');
    this.billsMap.set(id, bill);

    this.scheduleFlush();

    return id;
  }

  private getRange(from: number, to: number): Bill[] {
    if (from >= to) {
      throw new TypeError('`from` is greater than or equal to `to`');
    }

    // console.time('query');
    const start = binarySearchInsertPosition(
      this.bills,
      { time: from } as Bill,
      compareBillTime,
    );

    const end = binarySearchInsertPosition(
      this.bills,
      { time: to } as Bill,
      compareBillTime,
    );
    // console.timeEnd('query');

    // console.time('slice');
    const bills = this.bills.slice(start, end);
    // console.timeEnd('slice');

    return bills;
  }

  public async query(options: QueryOptions): Promise<{
    total: number;
    pageNo: number;
    pageSize: number;
    list: Bill[],
  }> {
    let bills = this.getRange(options.from, options.to);

    if (options.categories) {
      const s = new Set(options.categories);
      bills = bills.filter((bill) => s.has(getBillCategory(bill)));
    }

    const { pageNo = defaultPageNo, pageSize = defaultPageSize } = options;
    const list = bills.slice((pageNo - 1) * pageSize, pageNo * pageSize);

    return {
      total: bills.length,
      pageNo,
      pageSize,
      list,
    };
  }

  public async getSummary(from: number, to: number): Promise<Summary> {
    const bills = this.getRange(from, to);
    const summary = analyze(bills, this.categoriesMap);
    return summary;
  }

  // ----------------------- init ------------------------
  private async loadCategories(): Promise<void> {
    this.categories = await readCSV<Category>(this.categoriesCSVPath, {
      cast(value, { column }) {
        if (column === 'type') {
          return parseInt(value, 10);
        }
        return value;
      },
    });
    this.categoriesMap = this.categories
      .reduce((acc, cur) => acc.set(cur.id, cur), new Map());
  }

  private async loadBills(): Promise<void> {
    await readCSV(this.billCSVPath, {
      cast(value, { column }) {
        if (column === 'type' || column === 'time' || column === 'amount') {
          return parseInt(value, 10);
        }
        if (column === 'category') {
          return value || null;
        }
        return value;
      },
      onRecord: (record: Bill) => {
        this.bills.push(record);
        this.billsMap.set(record.id, record);
      },
    });

    // console.time('sort');
    this.bills.sort(compareBillTime);
    // console.timeEnd('sort');
  }


  // ----------------------- 数据落盘 ------------------------
  private readonly idleTime;

  private flushing: boolean;

  private pending: boolean;

  // eslint-disable-next-line no-undef
  private timeoutId: NodeJS.Timeout | null;

  // 新增数据空闲 ServerBillStore.idleTime 后，写入到磁盘
  private scheduleFlush() {
    if (this.flushing) {
      this.pending = true;
      return;
    }

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    this.timeoutId = setTimeout(async () => {
      this.timeoutId = null;

      this.flushing = true;
      // console.time('flush');
      await this.flush(); // todo: error log
      // console.timeEnd('flush');
      this.flushing = false;

      if (this.pending) {
        this.scheduleFlush();
        this.pending = false;
      }
    }, this.idleTime);
  }

  private flush(): Promise<void> {
    // 快照，防止异步新增数据导致不一致
    // console.time('slice');
    const snapshot = this.bills.slice();
    // console.timeEnd('slice');

    return flushBills(this.billCSVPath, snapshot);
  }
}
