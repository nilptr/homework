/* eslint-disable max-classes-per-file */

import PreAllocArray from './pre-alloc-array';

type GetKeyFunc<T> = (record: T) => number;
// type Comparator<T> = (a: T, b: T) => number;

// 二分搜索第一个 k 位置
export function binarySearch(arr: number[], k: number): number {
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    const mid = low + Math.floor((high - low) / 2);

    if (arr[mid] < k) {
      low = mid + 1;
    } else if (arr[mid] > k) {
      high = mid - 1;
    } else {
      let idx = mid;

      for (; idx > 0 && arr[idx - 1] === k; --idx);

      return idx;
    }
  }

  return -1;
}

// 二分搜索 k 插入位置（插入在相同 k 的后方）
export function binarySearchInsertPosition(arr: number[], k: number): number {
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    const mid = low + Math.floor((high - low) / 2);

    if (arr[mid] === k) {
      let idx = mid;
      for (; idx < arr.length && arr[idx] === k; ++idx);
      return idx;
    }

    if (arr[mid] < k) {
      low = mid + 1;
    } else if (arr[mid] > k) {
      high = mid - 1;
    }
  }

  return low; // = return low; ?
}

abstract class BPlusTreeNode<T> {
  protected readonly order: number;

  protected keys: number[];

  constructor(order: number) {
    this.order = order;

    this.keys = [];
  }

  public abstract size(): number;

  public abstract search(key: number): T[] | null;

  public abstract getAll(): T[];

  public abstract getRange(from: number, to: number): T[];

  public abstract insert(key: number, value: T): void;

  public abstract getFirstLeafKey(): number;

  public abstract isOverflow(): boolean;

  public abstract split(): BPlusTreeNode<T>;

  public abstract toString(): string;
}

class BPlusTreeLeafNode<T> extends BPlusTreeNode<T> {
  private values: T[][];

  private next: BPlusTreeLeafNode<T> | null;

  constructor(order: number) {
    super(order);

    this.values = [];
    this.next = null;
  }

  public size(): number {
    return this.values.reduce((acc, cur) => acc + cur.length, 0);
  }

  public insert(key: number, value: T): void {
    const bucket = this.search(key);

    if (bucket) {
      bucket.push(value);
      return;
    }

    const idx = binarySearchInsertPosition(this.keys, key);

    this.keys.splice(idx, 0, key);
    this.values.splice(idx, 0, [value]);
  }

  public search(key: number): T[] | null {
    const idx = binarySearch(this.keys, key);

    if (idx > -1) {
      return this.values[idx];
    }

    return null;
  }

  public getRange(from: number, to: number): T[] {
    const ret = [];

    let leaf: BPlusTreeLeafNode<T> | null = this;

    while (leaf !== null) {
      const { keys, values } = leaf;
      for (let i = 0; i < keys.length; ++i) {
        const key = keys[i];
        const value = values[i];

        // 提前结束
        if (key >= to) {
          return ret;
        }

        if (key >= from && key < to) {
          ret.push(...value);
        }
      }
      leaf = leaf.next;
    }

    return ret;
  }

  public getAll(): T[] {
    const all: T[] = [];

    this.values.forEach((value) => {
      all.push(...value);
    });

    return all;
  }

  public getFirstLeafKey(): number {
    return this.keys[0];
  }

  public isOverflow(): boolean {
    return this.keys.length >= this.order;
  }

  public split(): BPlusTreeNode<T> {
    const sibling = new BPlusTreeLeafNode<T>(this.order);

    const idx = Math.floor((this.keys.length + 1) / 2);

    sibling.keys.push(...this.keys.slice(idx));
    sibling.values.push(...this.values.slice(idx));

    this.keys = this.keys.slice(0, idx);
    this.values = this.values.slice(0, idx);

    sibling.next = this.next;
    this.next = sibling;

    return sibling;
  }

  // --------- 给 BPlusTreeInternalNode 使用 ---------
  public getNext(): BPlusTreeLeafNode<T> | null {
    return this.next;
  }

  public toString(): string {
    return `(${this.keys.join(', ')})`;
  }
}

class BPlusTreeInternalNode<T> extends BPlusTreeNode<T> {
  private children: BPlusTreeNode<T>[];

  constructor(order: number) {
    super(order);

    this.children = [];
  }

  // 方便生成 root 节点
  public build(nodes: BPlusTreeNode<T>[]): void {
    for (let i = 0; i < nodes.length; ++i) {
      const node = nodes[i];
      if (i > 0) {
        this.keys.push(node.getFirstLeafKey());
      }
      this.children.push(node);
    }
  }

  public size(): number {
    let count = 0;

    for (let i = 0; i < this.children.length; ++i) {
      count += this.children[i].size();
    }

    return count;
  }

  public insert(key: number, value: T): void {
    const idx = binarySearchInsertPosition(this.keys, key);

    const child = this.children[idx];

    child.insert(key, value);

    if (child.isOverflow()) {
      const sibling = child.split();

      this.keys.splice(idx, 0, sibling.getFirstLeafKey());
      this.children.splice(idx + 1, 0, sibling);
    }
  }

  public search(key: number): T[] | null {
    const idx = binarySearchInsertPosition(this.keys, key);

    const node = this.children[idx];

    return node.search(key);
  }

  public getRange(from: number, to: number): T[] {
    let child = this as BPlusTreeNode<T>;

    while (child instanceof BPlusTreeInternalNode) {
      const idx = binarySearchInsertPosition(child.keys, from);

      child = child.children[idx];
    }

    return child.getRange(from, to);
  }

  public getAll(): T[] {
    let firstChild = this.children[0];
    while (firstChild instanceof BPlusTreeInternalNode) {
      [firstChild] = firstChild.children;
    }

    let leafNode = firstChild as BPlusTreeLeafNode<T> | null;

    const buf = new PreAllocArray<T>(this.size());

    while (leafNode !== null) {
      buf.push(...leafNode.getAll());

      leafNode = leafNode.getNext();
    }

    return buf.getArray();
  }

  public getFirstLeafKey(): number {
    return this.children[0].getFirstLeafKey();
  }

  private getLeafNode(key: number): BPlusTreeLeafNode<T> {
    let child = this as BPlusTreeNode<T>;

    while (child instanceof BPlusTreeInternalNode) {
      const idx = binarySearchInsertPosition(child.keys, key);

      child = child.children[idx];
    }

    return child as BPlusTreeLeafNode<T>;
  }

  public isOverflow(): boolean {
    return this.keys.length >= this.order;
  }

  public split(): BPlusTreeNode<T> {
    const sibling = new BPlusTreeInternalNode<T>(this.order);

    const idx = Math.floor(this.keys.length / 2);

    sibling.keys.push(...this.keys.slice(idx + 1));
    sibling.children.push(...this.children.slice(idx + 1));

    this.keys = this.keys.slice(0, idx);
    this.children = this.children.slice(0, idx + 1);

    return sibling;
  }

  public toString(): string {
    const levels = [] as string[];

    let level: BPlusTreeNode<T>[] = [this];

    while (level.length > 0) {
      const tmp = [];

      const nextLevel: BPlusTreeNode<T>[] = [];

      for (let i = 0; i < level.length; ++i) {
        const node = level[i];

        if (node instanceof BPlusTreeInternalNode) {
          tmp.push(`{${node.keys.join(', ')}}`);

          nextLevel.push(...node.children);
        } else {
          tmp.push(node.toString());
        }
      }

      level = nextLevel;

      levels.push(tmp.join(' '));
    }

    return levels.join('\n');
  }
}

// https://en.wikipedia.org/wiki/B%2B_tree
export default class BPlusTree<T> {
  private readonly order: number; // B+ Tree order

  private readonly getKeyFn: GetKeyFunc<T>;

  private root: BPlusTreeNode<T>;

  constructor(order: number, getKeyFn: GetKeyFunc<T>) {
    if (order < 3) {
      throw new TypeError(`Illegal branching factor: ${order}`);
    }

    this.order = order;

    this.getKeyFn = getKeyFn;

    this.root = new BPlusTreeLeafNode(order);
  }

  public size(): number {
    return this.root.size();
  }

  public insert(value: T): void {
    const key = this.getKeyFn(value);

    this.root.insert(key, value);

    if (this.root.isOverflow()) {
      const sibling = this.root.split();

      const newRoot = new BPlusTreeInternalNode<T>(this.order);

      newRoot.build([this.root, sibling]);

      this.root = newRoot;
    }
  }

  public getRange(from: number, to: number): T[] {
    if (to <= from) {
      throw new TypeError('`from` is greater than or equal to `to`');
    }

    return this.root.getRange(from, to);
  }

  public getAll(): T[] {
    return this.root.getAll();
  }

  public toString(): string {
    return this.root.toString();
  }
}
