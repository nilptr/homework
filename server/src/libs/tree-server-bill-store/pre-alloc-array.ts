
export default class PreAllocArray<T> {
  private buffer: T[];

  private len: number;

  constructor(cap: number) {
    this.buffer = new Array(cap);
    this.len = 0;
  }

  public push(...items: T[]): number {
    if (items.length + this.len > this.buffer.length) {
      // console.log('realloc');
      this.buffer = this.buffer.concat(items);
      this.len = this.buffer.length;
      return this.len;
    }

    for (let i = 0; i < items.length; ++i) {
      const item = items[i];
      this.buffer[this.len] = item;
      this.len += 1;
    }

    return this.len;
  }

  public getArray(): T[] {
    if (this.len < this.buffer.length) {
      // console.log('realloc');
      return this.buffer.slice(0, this.len);
    }
    return this.buffer;
  }
}
