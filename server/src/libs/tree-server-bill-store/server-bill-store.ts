import * as fs from 'fs';

import { parse } from 'csv';
import { Options as ParseOptions } from 'csv-parse';
import { nanoid } from 'nanoid';

import {
  BillStore,
  Category,
  BillData,
  Bill,
  QueryOptions,
  Summary,
} from '../../types';
import BPlusTree from './b-plus-tree';
import analyze from '../analyze';

function readCSV<T>(path: string, options?: ParseOptions): Promise<T[]> {
  return new Promise((resolve, reject) => {
    const stream = parse({
      ...options,
      delimiter: ',',
      trim: true,
      columns: true,
      skipEmptyLines: true,
    }, (err, records) => {
      if (err) {
        reject(err);
      } else {
        resolve(records);
      }
    });

    fs.createReadStream(path, {
      encoding: 'utf-8',
    }).pipe(stream);
  });
}

const columns = ['id', 'type', 'time', 'category', 'amount'] as (keyof Bill)[];

function isEmpty(value: any): boolean {
  return value === null || value === undefined;
}

function generateBillRow(bill: Bill): string {
  return `${columns.map((k) => (isEmpty(bill[k]) ? '' : bill[k]))}\n`;
}

function getBillCategory(bill: Bill): string {
  return bill.category || (bill.type
    ? 'uncategorized-income'
    : 'uncategorized-outgo');
}

export default class ServerBillStore implements BillStore {
  private readonly billCSVPath: string;

  private readonly categoriesCSVPath: string;

  private billsMap: Map<string, Bill>;

  private billsTree: BPlusTree<Bill>;

  private categories: Category[];

  private categoriesMap: Map<string, Category>;

  constructor(billCSVPath: string, categoriesCSVPath: string) {
    this.billCSVPath = billCSVPath;
    this.categoriesCSVPath = categoriesCSVPath;

    this.categories = [];
    this.categoriesMap = new Map();

    this.billsMap = new Map<string, Bill>();
    this.billsTree = new BPlusTree<Bill>(512, (b: Bill) => b.time);
  }

  init(): Promise<any> {
    return Promise.all([
      this.loadBills(),
      this.loadCategories(),
    ]);
  }

  getCategories(): Category[] {
    return this.categories;
  }

  getCategoryName(id: string): string {
    return this.categoriesMap.get(id)?.name || '';
  }

  async addBill(data: BillData): Promise<string> {
    const id = nanoid();

    const bill = {
      ...data,
      id,
    };

    // 发起异步 IO
    const io = this.appendRecords([bill]);

    // 执行耗时同步操作
    this.billsMap.set(id, bill);
    // console.time('insert');
    this.billsTree.insert(bill);
    // console.timeEnd('insert');

    // 等待异步 IO 完成
    await io;

    return id;
  }

  public async query(options: QueryOptions): Promise<{
    total: number;
    pageNo: number;
    pageSize: number;
    list: Bill[],
  }> {
    let bills = this.billsTree.getRange(options.from, options.to);

    if (options.categories) {
      const s = new Set(options.categories);
      bills = bills.filter((bill) => s.has(getBillCategory(bill)));
    }

    const { pageNo = 1, pageSize = 10 } = options;
    const list = bills.slice((pageNo - 1) * pageSize, pageNo * pageSize);

    return {
      total: bills.length,
      pageNo,
      pageSize,
      list,
    };
  }

  public async getSummary(from: number, to: number): Promise<Summary> {
    const bills = this.billsTree.getRange(from, to);
    return analyze(bills, this.categoriesMap);
  }

  // ----------------------- init ------------------------

  private async loadCategories(): Promise<void> {
    this.categories = await readCSV<Category>(this.categoriesCSVPath, {
      cast(value, { column }) {
        if (column === 'type') {
          return parseInt(value, 10);
        }
        return value;
      },
    });
    this.categoriesMap = this.categories
      .reduce((acc, cur) => acc.set(cur.id, cur), new Map());
  }

  private async loadBills(): Promise<void> {
    await readCSV(this.billCSVPath, {
      cast(value, { column }) {
        if (column === 'type' || column === 'time' || column === 'amount') {
          return parseInt(value, 10);
        }
        if (column === 'category') {
          return value || null;
        }
        return value;
      },
      onRecord: (record: Bill) => {
        this.billsMap.set(record.id, record);
        this.billsTree.insert(record);
      },
    });
  }

  // ----------------------- 数据落盘 ------------------------

  private async appendRecords(records: Bill[]): Promise<void> {
    let text = '';

    for (let i = 0; i < records.length; ++i) {
      text += generateBillRow(records[i]);
    }

    // console.time('append');
    await fs.promises.appendFile(this.billCSVPath, text);
    // console.timeEnd('append');
  }
}
