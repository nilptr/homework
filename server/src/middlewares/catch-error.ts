import { Context, Next } from 'koa';

export default async function CatchError(ctx: Context, next: Next): Promise<void> {
  try {
    await next();
  } catch (err) {
    console.error(err);

    ctx.status = 500;

    if (process.env.NODE_ENV === 'production') {
      ctx.body = 'Internal Server Error';
    } else {
      ctx.body = err.stack;
    }
  }
}
