import * as Router from '@koa/router';

import { AppContext } from './types';

import {
  addBill,
  getBill,
  getCategories,
  getSummary,
} from './controllers';

const router = new Router<{}, AppContext>({ prefix: '/api/v1' });

router.get('/categories', getCategories);

router.post('/bills', addBill);

router.get('/bills', getBill);

router.get('/summary', getSummary);

export default router;
