
import { nanoid } from 'nanoid';

import { Bill, BillType } from '../types';

const dayMillis = 24 * 3600 * 1000;

// const columns = ['id', 'type', 'time', 'category', 'amount'];

// const categories = [
//   { id: '1bcddudhmh', type: 0, name: '车贷' },
//   { id: 'hc5g66kviq', type: 0, name: '车辆保养' },
//   { id: '8s0p77c323', type: 0, name: '房贷' },
//   { id: '0fnhbcle6hg', type: 0, name: '房屋租赁' },
//   { id: 'odrjk823mj8', type: 0, name: '家庭用品' },
//   { id: 'bsn20th0k2o', type: 0, name: '交通' },
//   { id: 'j1h1nohhmmo', type: 0, name: '旅游' },
//   { id: '3tqndrjqgrg', type: 0, name: '日常饮食' },

//   { id: 's73ijpispio', type: 1, name: '工资' },
//   { id: '1vjj47vpd28', type: 1, name: '股票投资' },
//   { id: '5il79e11628', type: 1, name: '基金投资' },
// ];

// const outgoCategories = categories.slice(0, -3);
// const incomeCategories = categories.slice(-3);

function randomNumber(min: number, max: number): number {
  return min + Math.floor(Math.random() * (max - min));
}

// function randomPick<T>(arr: T[]): T {
//   if (arr.length === 0) {
//     throw new TypeError('arr is empty');
//   }
//   return arr[randomNumber(0, arr.length)];
// }

export function generateSimulatedData(from: Date, to: Date): Bill[] {
  const ret = [];

  for (let day = from; day < to; day = new Date(day.getTime() + dayMillis)) {
    // 每月一笔工资收入 一笔房贷 车贷 租赁支出
    if (day.getDate() === 1) {
      ret.push({
        id: nanoid(),
        type: BillType.Income,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: 's73ijpispio',
        amount: 3000000,
      }, {
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: '8s0p77c323',
        amount: 1000000,
      }, {
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: '1bcddudhmh',
        amount: 500000,
      });
    }

    // 每周车辆保养
    if (day.getDay() === 6) {
      ret.push({
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: 'hc5g66kviq',
        amount: randomNumber(10000, 40000),
      });
    }

    // 每日基金股票
    if (Math.random() < 0.4) {
      ret.push({
        id: nanoid(),
        type: BillType.Income,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: '1vjj47vpd28',
        amount: randomNumber(0, 30000),
      });
    }
    if (Math.random() < 0.5) {
      ret.push({
        id: nanoid(),
        type: BillType.Income,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: '5il79e11628',
        amount: randomNumber(0, 20000),
      });
    }

    // 日常饮食
    const randomCount1 = randomNumber(1, 5);
    for (let i = 0; i < randomCount1; ++i) {
      ret.push({
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: '3tqndrjqgrg',
        amount: randomNumber(500, 5000),
      });
    }

    // 交通
    const randomCount2 = randomNumber(0, 4);
    for (let i = 0; i < randomCount2; ++i) {
      ret.push({
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: 'bsn20th0k2o',
        amount: randomNumber(300, 1000),
      });
    }

    // 旅游
    if (Math.random() < 0.05) {
      ret.push({
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: 'j1h1nohhmmo',
        amount: randomNumber(30000, 100000),
      });
    }

    if (Math.random() < 0.1) {
      ret.push({
        id: nanoid(),
        type: BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: 'odrjk823mj8',
        amount: randomNumber(1000, 5000),
      });
    }

    // 随机未归类记录
    const randomCount3 = randomNumber(0, 3);
    for (let i = 0; i < randomCount3; ++i) {
      ret.push({
        id: nanoid(),
        type: Math.random() < 0.5 ? BillType.Income : BillType.Outgo,
        time: day.getTime() + randomNumber(0, dayMillis),
        category: null,
        amount: randomNumber(500, 10000),
      });
    }
  }

  return ret;
}

export function generateRandomData(max: number): Bill[] {
  // todo:
  let size = 0;
  while (size < max) {
    size += 1;
  }
  return [];
}
