
import * as path from 'path';

import flushBills from '../libs/flush-bills';

import { generateSimulatedData } from './bill-generator';

(async () => {
  const start = new Date(2000, 0, 1);
  const end = new Date();

  const dataset = generateSimulatedData(start, end);

  const dist = path.resolve(__dirname, '../../bill.csv');

  await flushBills(dist, dataset);
})().catch(console.error);
