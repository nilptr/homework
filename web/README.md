# FE

```
./src
├── App.vue
├── assets
│   ├── fonts/
│   └── logo.png
├── components                        # 通用组件，不含业务逻辑
│   ├── ...
│   └── xm-switch.vue
├── index.scss                        # 全局样式
├── libs                              # 公共函数
│   ├── ...
│   └── format-date.ts
├── main.ts                           # 入口逻辑
├── pages                             # 页面级组件
│   ├── index
│   │   ├── components                # 业务逻辑相关组件
│   │   │   ├── add-bill-dialog.vue
│   │   │   └── bill-list.vue
│   │   └── index.vue
│   └── loading
│       └── index.vue
├── shims-vue.d.ts                    # vue typescript 支持
└── types.ts                          # 类型定义
```
