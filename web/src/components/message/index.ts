import { App, createApp } from 'vue';

import MessageManager from './manager.vue';

export interface MessageOptions {
  type?: 'info' | 'success' | 'error' | 'warning';
  text: string;
  duration?: number;
}

interface MessageManagerInstance {
  add(options: MessageOptions): number;
  remove(id: number): void;
}

let root: HTMLDivElement | null;
let app: App | null;
let manager: MessageManagerInstance | null;

function init() {
  root = document.createElement('div');

  document.body.appendChild(root);

  app = createApp(MessageManager);

  manager = app.mount(root) as unknown as MessageManagerInstance;
}

export function destroy() {
  if (app) {
    manager = null;
    app.unmount(root);
    document.body.removeChild(root!);
    root = null;
  }
}

function $message(options: MessageOptions): number {
  if (!manager) {
    init();
  }
  return manager!.add(options);
}

export default $message;
