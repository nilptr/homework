import axios, {
  AxiosInstance,
  AxiosResponse,
  CancelTokenSource,
} from 'axios';

import {
  // Bill,
  BillData,
  BillStore,
  Category,
  QueryOptions,
  QueryResult,
  Summary,
} from '../types';

interface Response<T> {
  code: number,
  msg?: string,
  data?: T,
}

const { CancelToken } = axios;

const networkErrorMessage = '网络异常，请检查网络连接或稍后重试';

function getResponseData<T>(res: AxiosResponse<Response<T>>): T {
  const { code, msg, data } = res.data;

  if (code !== 0) {
    throw new Error(msg);
  }

  return data!;
}

function sleep(time: number): Promise<any> {
  return new Promise((resolve) => setTimeout(resolve, time));
}

class WebBillStore implements BillStore {
  private prefix: string;

  private axios: AxiosInstance;

  private categories: Category[];

  private categoriesMap: Map<string, Category>;

  private querySource: CancelTokenSource | null;

  private summarySource: CancelTokenSource | null;

  constructor(prefix: string) {
    this.prefix = prefix;

    this.axios = axios.create({
      baseURL: prefix,
    });

    this.categories = [];
    this.categoriesMap = new Map();

    this.querySource = null;
    this.summarySource = null;
  }

  async init(): Promise<void> {
    const res = await this.axios.get('/categories');

    try {
      const data = getResponseData<Category[]>(res);

      this.categories = data!;
      this.categoriesMap = data!
        .reduce((acc, cur) => acc.set(cur.id, cur), new Map());
    } catch (err) {
      throw new Error(networkErrorMessage);
    }
  }

  getCategories(): Category[] {
    return this.categories;
  }

  getCategoryName(id: string): string {
    return this.categoriesMap.get(id)?.name || '';
  }

  async addBill(data: BillData): Promise<string> {
    const res = await this.axios.post('/bills', data);

    try {
      return getResponseData<string>(res);
    } catch (err) {
      throw new Error(networkErrorMessage);
    }
  }

  async query(options: QueryOptions): Promise<QueryResult> {
    if (this.querySource) {
      this.querySource.cancel();
      this.querySource = null;
    }

    this.querySource = CancelToken.source();

    let res: AxiosResponse;

    try {
      res = await this.axios.get('/bills', {
        params: {
          ...options,
          categories: options.categories ? options.categories.join(',') : null,
        },
        cancelToken: this.querySource.token,
      });
    } catch (err) {
      throw new Error(networkErrorMessage);
    }

    return getResponseData<QueryResult>(res);
  }

  async getSummary(from: number, to: number): Promise<Summary> {
    if (this.summarySource) {
      this.summarySource.cancel();
      this.summarySource = null;
    }

    this.summarySource = CancelToken.source();

    let res: AxiosResponse;

    try {
      res = await this.axios.get('/summary', {
        params: { from, to },
        cancelToken: this.summarySource.token,
      });
    } catch (err) {
      throw new Error(networkErrorMessage);
    }

    return getResponseData<Summary>(res);
  }
}

export default new WebBillStore('/api/v1');
