/* eslint-disable quote-props */

const colorMap = {
  // 收入
  '工资': '#5B8FF9',
  '基金投资': '#64bfe8',
  '股票投资': '#61DDAA',

  '交通': '#F6BD16',
  '家庭用品': '#FFE0ED',
  '日常饮食': '#F6903D',
  '旅游': '#FFE0C7',
  '房屋租赁': '#e56b56',
  '房贷': '#FF6B3B',
  '车贷': '#fbe8ab',
  '车辆保养': '#F08BB4',
} as { [key: string]: string };

export default function getColor({ name }: { name: string }): string {
  return colorMap[name] || '#C0C4CC';
}
