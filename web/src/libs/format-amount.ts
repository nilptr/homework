export default function formatAmount(amount: number): string {
  const str = `${amount}`;

  const integer = str.slice(0, -2) || '0';
  const decimal = `00${str}`.slice(-2);

  const arr = [];

  const remainder = integer.length % 3;
  if (remainder > 0) {
    arr.push(integer.slice(0, remainder));
  }

  for (let i = remainder; i < integer.length; i += 3) {
    arr.push(integer.slice(i, i + 3));
  }

  return `${arr.join(',')}.${decimal}`;
}
