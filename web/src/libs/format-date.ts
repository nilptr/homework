
export function formatDate(date: Date): string {
  const yyyy = `${date.getFullYear()}`;
  const MM = `00${date.getMonth() + 1}`.slice(-2);
  const dd = `00${date.getDate()}`.slice(-2);
  return `${yyyy}-${MM}-${dd}`;
}

export function formatTime(date: Date): string {
  const hh = `00${date.getHours()}`.slice(-2);
  const mm = `00${date.getMinutes()}`.slice(-2);
  return `${hh}:${mm}`;
}

export function formateDateTime(date: Date): string {
  return `${formatDate(date)} ${formatTime(date)}`;
}
