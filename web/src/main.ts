import { createApp } from 'vue';

import App from './App.vue';

import './assets/fonts/iconfont.css';

import './index.scss';

// const modalLayer = document.createElement('div');
// modalLayer.id = '#modal-layer';


createApp(App).mount('#app');
