// shims-vue.d.ts
// to import .vue files in .ts files:
// https://github.com/vuejs/vue-next/issues/990
declare module '*.vue' {
  import { Component } from 'vue';

  const component: Component;
  export default component;
}

declare module '*.svg' {
  const url: string;
  export default url;
}
