// https://stackoverflow.com/questions/42233987/how-to-configure-custom-global-interfaces-d-ts-files-for-typescript
// https://www.typescriptlang.org/tsconfig#isolatedModules

export enum BillType {
  Outgo = 0,
  Income = 1,
}

export interface BillData {
  time: number,
  type: BillType,
  category?: string | null,
  amount: number,
}

export interface Bill extends BillData {
  id: string,
}

export interface Category {
  id: string;
  type: BillType,
  name: string,
}

export interface QueryOptions {
  from: number;
  to: number;

  categories?: string[] | null;

  pageNo?: number;
  pageSize?: number;
}

export interface QueryResult {
  total: number;
  pageNo: number;
  pageSize: number;
  list: Bill[];
}

export interface TypeSummary {
  amount: number;
  categories: {
    id: string;
    name: string;
    amount: number;
  }[];
}

export interface Summary {
  income: TypeSummary;
  outgo: TypeSummary;
}

export interface BillStore {
  // 读取 categories
  // or 缓存 bill 数据
  init(): Promise<void>;

  getCategories(): Category[];

  getCategoryName(id: string): string;

  // 添加账单，返回 id
  addBill(data: BillData): Promise<string>;

  // 批量添加
  bulkAddBill?(list: BillData[]): Promise<string[]>;

  query(options: QueryOptions): Promise<QueryResult>;

  getSummary(from: number, to: number): Promise<Summary>;
}
